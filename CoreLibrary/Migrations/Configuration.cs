using CoreLibrary.Models;

namespace CoreLibrary.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DAL.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DAL.DataContext context)
        {
            context.Companies.AddOrUpdate(company => company.Name, new Company() { Name = "Unpublished company"});
            context.SaveChanges();
        }
    }
}
