﻿using System.Data.Entity;
using CoreLibrary.Models;

namespace CoreLibrary.DAL
{
    public class DataContext : DbContext
    {
        public DataContext() : base("name=DBConnection")
        {
            //Database.SetInitializer(new DropCreateDatabaseAlways<DataContext>()); // for recreating physical database file
            Configuration.AutoDetectChangesEnabled = false;
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Company> Companies { get; set; }
    }
}
