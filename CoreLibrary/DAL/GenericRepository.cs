﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace CoreLibrary.DAL
{
    public interface IGenericRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetById(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Delete(T entity);
        void Edit(T entity);
        void Save();
    }

    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class 
    {
        private readonly DataContext _entities;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(DataContext context)
        {
            _entities = context;
            _dbSet = context.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            IQueryable<TEntity> query = _dbSet.AsQueryable();
            return query;
        }

        public virtual IQueryable<TEntity> GetById(Expression<Func<TEntity, bool>> predicate)
        {
            IQueryable<TEntity> query = _dbSet.Where(predicate).AsQueryable();
            return query;
        }

        public virtual void Add(TEntity entity)
        {
            _dbSet.Add(entity);
            _entities.Entry(entity).State = EntityState.Added;
        }

        public virtual void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
            _entities.Entry(entity).State = EntityState.Deleted;
        }

        public virtual void Edit(TEntity entity)
        {
            _entities.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }
    }
}