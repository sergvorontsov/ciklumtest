﻿namespace CoreLibrary.DTO
{
    public class CompanyDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}