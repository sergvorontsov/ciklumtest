﻿namespace CoreLibrary.DTO
{
    public class UserDto 
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int? CompanyId { get; set; }
    }
}
