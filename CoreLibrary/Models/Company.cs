﻿using System.Collections.Generic;

namespace CoreLibrary.Models
{
    public class Company : BaseObject
    {

        public ICollection<User> Users { get; set; }

        public Company()
        {
            Users = new List<User>();
        }

        public static int DefaultId { get ; set; }
    }
}
