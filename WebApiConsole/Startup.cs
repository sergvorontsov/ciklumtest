﻿using Owin;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using CoreLibrary.DAL;
using Microsoft.Practices.Unity;
using WebApiConsole.Filters;

namespace WebApiConsole
{
    public class Startup
    {
        // This code configures Web API. The Startup class is specified as a type parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Services.Add(typeof(IExceptionLogger), new ExceptionManagerApi());

            // Dependency Resolver
            var container = new UnityContainer();

            container.RegisterType(typeof(IDefaultCompany), typeof(DefaultCompany), new HierarchicalLifetimeManager());
            container.RegisterType(typeof(IGenericRepository<>), typeof(GenericRepository<>), new HierarchicalLifetimeManager());

            config.DependencyResolver = new UnityResolver(container);

            appBuilder.UseWebApi(config);
        }
    }
}