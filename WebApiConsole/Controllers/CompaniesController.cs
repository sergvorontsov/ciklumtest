﻿using System.Linq;
using System.Web.Http;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using CoreLibrary.DAL;
using CoreLibrary.DTO;
using CoreLibrary.Models;
using log4net;

namespace WebApiConsole.Controllers
{
    [AllowAnonymous]
    public class CompaniesController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IGenericRepository<Company> _companyRepository;

        public CompaniesController(IGenericRepository<Company> companyRepository)
        {
            _companyRepository = companyRepository;
            Mapper.Initialize(cfg => cfg.CreateMap<Company, CompanyDto>());
        }

        // GET api/companies
        public IQueryable<CompanyDto> GetCompanies()
        {
            Log.Debug($"Called GetCompanies()");
            return _companyRepository.GetAll().ProjectTo<CompanyDto>().AsQueryable();
        }

        // GET api/companies/5 
        public CompanyDto GetCompany(int id)
        {
            Log.Debug($"Called GetCompany({id})");
            return Mapper.Map<CompanyDto>(_companyRepository.GetById(company => company.Id == id).First());
        }

        // POST api/companies
        [HttpPost]
        public CompanyDto CreateCompany([FromBody]Company value)
        {
            Log.Debug($"Called CreateCompany({value.Name})");
            _companyRepository.Add(value);
            _companyRepository.Save();
            return Mapper.Map<CompanyDto>(value);
        }

        // PUT api/companies/5 
        [HttpPut]
        public CompanyDto EditCompany(int id, [FromBody]Company value)
        {
            Log.Debug($"Called EditCompany({value.Name})");
            _companyRepository.Add(value);
            _companyRepository.Save();
            return Mapper.Map<CompanyDto>(value);
        }

        // DELETE api/companies/5 
        public IHttpActionResult DeleteCompany(int id)
        {
            Log.Debug($"Called DeleteCompany({id})");
            var entity = _companyRepository.GetById(company => company.Id == id).First();
            if (entity != null)
            {
                _companyRepository.Delete(entity);
                _companyRepository.Save();
            }
            return Ok();
        }
    }
}