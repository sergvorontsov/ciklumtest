﻿using System.Linq;
using System.Web.Http;
using CoreLibrary.DAL;
using CoreLibrary.DTO;
using CoreLibrary.Models;
using log4net;
using WebApiConsole.Filters;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace WebApiConsole.Controllers
{
    [AllowAnonymous]
    public class UsersController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IGenericRepository<User> _userRepository;

        public UsersController(IGenericRepository<User> userRepository)
        {
            _userRepository = userRepository;
            Mapper.Initialize(cfg => cfg.CreateMap<User, UserDto>());
        }
        // GET api/users
        public IQueryable<UserDto> GetUsers()
        {
            Log.Debug($"Called GetUsers()");
            return _userRepository.GetAll().ProjectTo<UserDto>().AsQueryable();
        }


        // GET api/users/5 
        public UserDto GetUser(int id)
        {
            Log.Debug($"Called GetUser({id})");
            return Mapper.Map<UserDto>(_userRepository.GetById(user => user.Id == id).First());
        }

        // POST api/users
        [HttpPost]
        [UserCompanyDefaultFilter]
        public UserDto CreateUser([FromBody]User value)
        {
            Log.Debug($"Called CreateUser({value.Name})");
            _userRepository.Add(value);
            _userRepository.Save();
            return Mapper.Map<UserDto>(value);
        }

        // PUT api/users/5 
        [HttpPut]
        public UserDto EditUser(int id, [FromBody]User value)
        {
            Log.Debug($"Called EditUser({value.Name})");
            _userRepository.Add(value);
            _userRepository.Save();
            return Mapper.Map<UserDto>(value);
        }

        // DELETE api/users/5 
        [HttpDelete]
        public IHttpActionResult DeleteUser(int id)
        {
            Log.Debug($"Called DeleteUser({id})");
            var entity = _userRepository.GetById(user => user.Id == id).First();
            if (entity != null)
            {
                _userRepository.Delete(entity);
                _userRepository.Save();
            }
            return Ok();
        }
    }
}
