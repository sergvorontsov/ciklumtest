﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using CoreLibrary.Models;

namespace WebApiConsole.Filters
{
    [AttributeUsage(AttributeTargets.Method)]
    public class UserCompanyDefaultFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var requestScope = actionContext.Request.GetDependencyScope();
            var service = requestScope.GetService(typeof(DefaultCompany)) as IDefaultCompany;

            foreach (var argument in actionContext.ActionArguments.Values.Where(v => v is User))
            {
                User user = argument as User;
                if (user != null && !user.CompanyId.HasValue)
                {
                    service?.DefaultCompanyId((User)argument);
                }
            }
        }

    }
}