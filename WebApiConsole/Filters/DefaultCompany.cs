﻿using System.Linq;
using CoreLibrary.DAL;
using CoreLibrary.Models;

namespace WebApiConsole.Filters
{
    public interface IDefaultCompany
    {
        void DefaultCompanyId(User user);
    }

    public class DefaultCompany : IDefaultCompany
    {
        private readonly IGenericRepository<Company> _companiesRepository;

        public DefaultCompany(IGenericRepository<Company> companiesRepository)
        {
            _companiesRepository = companiesRepository;
        }

        public void DefaultCompanyId(User user)
        {
            if (user == null || user.CompanyId != null)
                return;

            user.CompanyId = _companiesRepository.GetAll().First().Id;
        }
    }
}