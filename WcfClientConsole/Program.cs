﻿using System;
using log4net.Config;
using WcfClientConsole.WcfServiceRef;

[assembly: XmlConfigurator(Watch = true)]
namespace WcfClientConsole
{
    class Program
    {

        static void Main()
        {

            WcfServiceClient client = new WcfServiceClient("BasicHttpBinding_IWcfService");

            string sHeader = "- Tester Console. Copyright ;), 2017 (c), All rights reserved.\r\n" +
                             $"- Type 'help' or 'h' command for help. 'quit' or 'exit' for exit";
            
            Console.WriteLine(sHeader);

            bool bQuit = false;
            while (!bQuit)
            {
                Console.Write(">");
                String readLine = Console.ReadLine();
                //if (!String.IsNullOrEmpty(readLine) && Enum.TryParse(readLine.ToLower(), out command))
                if (!String.IsNullOrEmpty(readLine))
                {
                    Command command;
                    if (Enum.TryParse(readLine.ToLower(), out command))
                    {

                        switch (command)
                        {
                            case Command.help:
                            case Command.h:
                                Console.WriteLine(
                                    "\t'help' - this command\r\n"
                                    + "\t'quit', 'exit' - use this for close and exit from console\r\n"
                                    + "\t'name' - use this command for enter name and test application."
                                );
                                break;

                            case Command.name:
                                Console.Write($"Enter name:");
                                readLine = Console.ReadLine();
                                string sResultAddedUser = client.AddUser(readLine);
                                Console.WriteLine(sResultAddedUser);
                                break;

                            case Command.exit:
                            case Command.quit:
                                bQuit = true;
                                break;

                            case Command.clear:
                                Console.Clear();
                                Console.WriteLine(sHeader);
                                break;

                            default:
                                Console.WriteLine($"Unknown command. Use 'h' or 'help' command.");
                                break;
                        }
                    }
                    else
                        Console.WriteLine($"Unknown command. Use 'h' or 'help' command.");
                }
            }

        }

        enum Command
        {
            h,
            help,
            name,
            exit,
            quit,
            clear
        }

    }
}
