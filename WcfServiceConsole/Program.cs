﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using log4net;
using log4net.Config;
using WcfServiceConsole.WcfContracts;

[assembly: XmlConfigurator(Watch = true)]
namespace WcfServiceConsole
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main()
        {
            try
            {
                Log.Debug("Entering application.");

                Uri httpBaseAddress = new Uri("http://localhost:8799/WcfService"); // Base Address

                using (ServiceHost host = new ServiceHost(typeof(WcfService), httpBaseAddress))
                {
                    ServiceMetadataBehavior smb = new ServiceMetadataBehavior
                    {
                        HttpGetEnabled = true,
                        MetadataExporter = {PolicyVersion = PolicyVersion.Policy15}
                    };
                    host.Description.Behaviors.Add(smb);

                    host.Open();

                    Console.WriteLine(@"The service is ready at {0}", httpBaseAddress);
                    Console.WriteLine(@"Press <Enter> to stop the service.");
                    Console.ReadLine();

                    // Close the ServiceHost.
                    host.Close();
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Have you an exception. You should view log for detail...\r\nApplication stopped");
                Log.InfoFormat("Have an exception:\r\n" + ex.Message);
            }
        }
    }
}
