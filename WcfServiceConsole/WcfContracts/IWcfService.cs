﻿using System.ServiceModel;

namespace WcfServiceConsole.WcfContracts
{
    [ServiceContract]
    public interface IWcfService
    {
        [OperationContract]
        string AddUser(string value);

    }
}
