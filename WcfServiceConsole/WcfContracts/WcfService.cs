﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using CoreLibrary.DTO;
using log4net;
using Newtonsoft.Json;

namespace WcfServiceConsole.WcfContracts
{
    public class WcfService : IWcfService
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
        string baseAddress = "http://localhost:8800/";
        readonly HttpClient _client = new HttpClient();

        public string AddUser(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException(nameof(value));
            }
            Log.Info($"WCF Method called with {value} value.");

            var json = JsonConvert.SerializeObject(new UserDto() {Name = value});

            var response = _client.PostAsync(baseAddress + "api/users", new StringContent(json, Encoding.UTF8, "application/json")).Result;
            Log.Info($"Post response: {response}");
            Log.Info($"{response.Content.ReadAsStringAsync().Result}");

            StringBuilder sBuilder = new StringBuilder();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                UserDto user = JsonConvert.DeserializeObject<UserDto>(response.Content.ReadAsStringAsync().Result);

                sBuilder.Append($"New user was added. \r\n\tUser ID: {user.Id}\r\n\tName : {user.Name}\r\n\tCompany : {user.CompanyId}");
            }
            else
                sBuilder.Append($"Something wrong. User hasn't added");
            return sBuilder.ToString();
        }
    }
}
